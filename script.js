// let arr = [{ uuid: 1, value: 'help me', status: 'pending' },{ uuid: 1, value: 'help dmhf kdh me', status: 'pending' }]
let arr = []

function renderTodos (arr) {
  const list = document.getElementById('list')
  list.innerHTML = ''
  console.log(arr)

  arr.forEach(todo => {
    const itemDiv = createItemDiv(
      todo.id,
      todo.value,
      todo.status === 'completed'
    )
    list.appendChild(itemDiv)
  })
  // console.log(arr)

  updateCount()
}

function addEventListeners () {
  document.getElementById('textInput').addEventListener('keydown', todos)
  document.getElementById('allBtn').addEventListener('click', displayAllTodos)
  document
    .getElementById('activeBtn')
    .addEventListener('click', displayActiveTodos)
  document
    .getElementById('completedBtn')
    .addEventListener('click', displayCompletedTodos)
  document
    .getElementById('clearBtn')
    .addEventListener('click', clearCompletedTodos)
}

function todos (event) {
  if (event.key === 'Enter') {
    event.preventDefault()
    addItem()
    document.getElementById('footer').style.display = 'block'
    updateCount()
    document.getElementsByClassName('btn_header')[0].style.display = 'block'
    selectAllTodo()
  }
}

function addItem () {
  const text = document.getElementById('textInput').value.trim()
  if (text !== '') {
    const uuid = uuidv4()
    const newItem = {
      id: uuid,
      value: text,
      status: 'pending'
    }

    arr.push(newItem)
    renderTodos(arr)
    document.getElementById('textInput').value = ''
  }
}

function createItemDiv (uuid, text, completed) {
  const itemDiv = document.createElement('div')
  itemDiv.setAttribute('data-uuid', uuid)
  itemDiv.classList.add('item')

  const checkbox = document.createElement('input')
  checkbox.setAttribute('type', 'checkbox')
  checkbox.classList.add('checkbox')
  checkbox.checked = completed

  checkbox.addEventListener('change', function () {
    updateStatusInArray(itemDiv, uuid, checkbox.checked)
  })

  const labelElement = document.createElement('label')
  labelElement.setAttribute('class', 'editable_label')
  labelElement.textContent = text
  labelElement.addEventListener('dblclick', function () {
    editLabel(labelElement, itemDiv)
  })

  const deleteBtn = createDeleteButton(itemDiv)

  itemDiv.appendChild(checkbox)
  itemDiv.appendChild(labelElement)
  itemDiv.appendChild(deleteBtn)
  return itemDiv
}

// editing part
function editLabel (labelElement, itemDiv) {
  const editText = document.createElement('input')
  editText.setAttribute('type', 'text')
  editText.setAttribute('class', 'editText')
  editText.value = labelElement.textContent
  itemDiv.replaceChild(editText, labelElement)

  editText.focus()

  editText.addEventListener('keydown', function (event) {
    if (event.key === 'Enter') {
      const newText = editText.value.trim()
      labelElement.textContent = newText
      //arr editing part
      const uuid = itemDiv.dataset.uuid
      const index = arr.findIndex(item => item.id === uuid)
      if (index !== -1) {
        arr[index].value = newText
      }
      renderTodos(arr)
    }
  })
  console.log('updatetxt', arr)
}
const checkbox = document.createElement('input')
checkbox.setAttribute('type', 'checkbox')
checkbox.classList.add('checkbox')

function createDeleteButton (itemDiv) {
  const deleteBtn = document.createElement('button')
  deleteBtn.innerHTML = 'X'
  deleteBtn.classList.add('deleteBtn')
  deleteBtn.addEventListener('click', function () {
    const uuid = itemDiv.dataset.uuid
    arr = arr.filter(item => item.id !== uuid)
    renderTodos(arr)
    // console.log('delete', arr)
    updateCount()
  })
  return deleteBtn
}

// update status of the input wheater checked or not
function updateStatusInArray (itemDiv, uuid, completed) {
  const index = arr.findIndex(item => item.id === uuid)
  if (index !== -1) {
    arr[index].status = completed ? 'completed' : 'pending'
  }
  console.log('updateStatusInArray', arr)

  updateCount()
}

function updateCount () {
  const checkboxes = document.querySelectorAll('.checkbox')
  let count = checkboxes.length
  checkboxes.forEach(checkbox => {
    const label = checkbox.nextElementSibling
    if (checkbox.checked) {
      count--
      label.style.textDecoration = 'line-through'
    } else {
      label.style.textDecoration = 'none'
    }
  })
  document.getElementById('count').textContent = count
}

function selectAllTodo () {
  let isChecked = false
  const btnHeaders = document.getElementsByClassName('btn_header')

  for (let i = 0; i < btnHeaders.length; i++) {
    btnHeaders[i].addEventListener('click', function () {
      const checkboxes = document.querySelectorAll('.checkbox')
      isChecked = !isChecked

      checkboxes.forEach(checkbox => {
        checkbox.checked = isChecked
        const uuid = checkbox.parentElement.dataset.uuid

        // arr update part
        updateStatusInArray(checkbox.parentElement, uuid, isChecked)
      })

      updateCount()
    })
  }
}

function displayAllTodos () {
  renderTodos(arr)
  // const checkboxes = document.querySelectorAll('.checkbox')
  // checkboxes.forEach(checkbox => {
  //   const itemDiv = checkbox.parentElement
  //   itemDiv.style.display = 'flex'
  // })
}

function displayActiveTodos () {
  res = arr.filter(ele => ele.status === 'pending')
  renderTodos(res)
  console.log(res)

  // const checkboxes = document.querySelectorAll('.checkbox')
  // })
  // checkboxes.forEach(checkbox => {
  //   const itemDiv = checkbox.parentElement
  //   if (checkbox.checked) {
  //     itemDiv.style.display = 'none'
  //   } else {
  //     itemDiv.style.display = 'flex'
  //   }
  // })
}

function displayCompletedTodos () {
  res = arr.filter(ele => ele.status === 'completed')
  renderTodos(res)
  // console.log(res)

  // const checkboxes = document.querySelectorAll('.checkbox')
  // checkboxes.forEach(checkbox => {
  //   const itemDiv = checkbox.parentElement
  //   if (checkbox.checked) {
  //     itemDiv.style.display = 'flex'
  //   } else {
  //     itemDiv.style.display = 'none'
  //   }
  // })
}

function clearCompletedTodos () {
  arr = arr.filter(ele => ele.status !== 'completed')
  renderTodos(arr)
  // const checkboxes = document.querySelectorAll('.checkbox')
  // checkboxes.forEach(checkbox => {
  //   if (checkbox.checked) {
  //     const itemDiv = checkbox.parentElement
  //     itemDiv.remove()
  //   }
  // })
  updateCount()
}

addEventListeners()
renderTodos(arr)
